from fastapi import FastAPI, APIRouter, File, UploadFile
from models.garment import GarmentBase
from schema.schemas import list_serial
from config.database import collection_name
from bson import ObjectId
from fastapi.middleware.cors import CORSMiddleware
import numpy as np
from io import BytesIO
from PIL import Image
import tensorflow as tf

app = FastAPI()
router = APIRouter()

origins = [
    "http://localhost",
    "http://localhost:3000",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Model
MODEL = tf.keras.models.load_model('model/garment.h5')
CLASS_NAMES = ["defect3", "non-defect", "open-defect", "wavy-seam-defect"]


def read_file_as_image(data) -> np.ndarray:
    image = Image.open(BytesIO(data))
    # Resize image to (256, 256)
    image = image.resize((256, 256))
    image = np.array(image)
    return image


@router.post("/predict")
async def predict(file: UploadFile = File(...)):
    image = read_file_as_image(await file.read())
    img_batch = np.expand_dims(image, 0)
    
    predictions = MODEL.predict(img_batch)

    predicted_class = CLASS_NAMES[np.argmax(predictions[0])]
    confidence = np.max(predictions[0])
    return {
        'class': predicted_class,
        'confidence': float(confidence)
    }


# Get all todos
@router.get("/")
async def get_garment_base():
    garment = list_serial(collection_name.find())
    return garment


# Post a todo
@router.post("/")
async def post_garment_base(garment_base: GarmentBase):
    collection_name.insert_one(garment_base.dict())
    return {"message": "garment created successfully"}


# Put todo
@router.put("/{id}")
async def put_garment_base(id: str, garment_base: GarmentBase):
    collection_name.find_one_and_update({"_id": ObjectId(id)}, {"$set": garment_base.dict()})


# Delete todo
@router.delete("/{id}")
async def delete_garment_base(id: str):
    collection_name.find_one_and_delete({"_id": ObjectId(id)})


# Mount the router
app.include_router(router)

# Run the application
if __name__ == "__main__":
    import uvicorn

    # uvicorn.run(app, host="0.0.0.0", port=8000)
    uvicorn.run(app, host='localhost', port=8000)
