def individual_serial(garment)->dict:
    return {
        'id': str(garment['_id']),
        'candidate': garment['candidate'],
        'name': garment['name'],	
        'age': garment['age'],
        'joinDate': garment['joinDate'],
        'rate': garment['rate'],
        'completed': garment['completed']
       
    }
def list_serial(garment)->list:
    return [individual_serial(garment) for garment in garment]