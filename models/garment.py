from pydantic import BaseModel



class GarmentBase(BaseModel):

    candidate: str

    name: str 

    age: str

    joinDate: str

    rate: str

    completed: bool